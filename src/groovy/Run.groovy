import org.apache.commons.lang.StringUtils

/**
 * Created by sergey on 10/27/14.
 */

def path = args[0];

FileService fileService = new FileService();
ProcessService processService = new ProcessService();

File file = fileService.getFile(path);
processService.processFile(file);
fileService.printFile(file);







