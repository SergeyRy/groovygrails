import groovy.transform.Canonical
import org.apache.commons.lang.StringUtils

import javax.sound.sampled.Line
import java.text.DateFormat
import java.text.SimpleDateFormat

/**
 * Created by sergey on 10/27/14.
 */

@Canonical
class ProcessService {

    void processFile(File file) {
        if (file.name.substring(0,2) == "a_") {
            calculateNumberOfA(file);
        }
        if (file.name.substring(0,2) == "1_") {
            replaceContentWithMatrix(file)
        }
        if (file.name.substring(0,2) == "d_") {
            replaceContentWithData(file)
        }
    }

    void calculateNumberOfA(File file) {
        int countA = StringUtils.countMatches(file.getText(), "a");
        file << "\ncount = " + countA.toString();
    }

    void replaceContentWithMatrix(File file) {
        def matrix = [[1, 2, 3, 10], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]];
        file.write("");
        matrix.each {
            file << it[0] + " " +  it[1] + " " +  it[2] + " " +  it[3] + System.getProperty("line.separator")
        }

        file << "\nsum = " + getSumMainDiagonal(matrix).toString();
        file << "\nsum = " + getSumSideDiagonal(matrix).toString();

    }

    void replaceContentWithData(File file) {
        file.write("");
        file << String.format('%tF %<tT', new Date())
    }

    int getSumMainDiagonal(List matrix) {
        def sum = 0;
        matrix.eachWithIndex() { def obj, int i ->
            obj.eachWithIndex{ def obj2, int j ->
                if (i == j) {
                    sum = sum + obj2
                }
            }
        };
        return sum;
    }

    int getSumSideDiagonal(List matrix) {
        def sum = 0;
        def n = matrix.size();
        for (int i = 0; i < n; i++)
        {
            sum += matrix[i][n-1-i];
        }
        return sum;
    }


}
