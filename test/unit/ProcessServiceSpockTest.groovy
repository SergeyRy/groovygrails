import spock.lang.Specification

/**
 * Created by sergey on 10/29/14.
 */
class ProcessServiceSpockTest extends Specification {
    def ProcessService processService = new ProcessService();

    def "check sum main diagonal of matrix"() {
        given:
        def matrix = [[1, 2, 3, 10], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]];
        def matrix1 = [[2, 2, 3, 10], [5, 7, 7, 8], [9, 10, 12, 12], [13, 14, 15, 17]];
        def matrix2 = [[3, 2, 3, 10], [5, 8, 7, 8], [9, 10, 13, 12], [13, 14, 15, 18]];

        expect:
        processService.getSumMainDiagonal(matrix) == 34
        processService.getSumMainDiagonal(matrix1) == 38
        processService.getSumMainDiagonal(matrix2) == 42
    }

    def "check sum side diagonal of matrix"() {
        given:
        def matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]];
        def matrix1 = [[1, 2, 3, 5], [5, 6, 8, 8], [9, 11, 11, 12], [14, 14, 15, 16]];
        def matrix2 = [[1, 2, 3, 6], [5, 6, 9, 8], [9, 12, 11, 12], [15, 14, 15, 16]];

        expect:
        processService.getSumSideDiagonal(matrix) == 34
        processService.getSumSideDiagonal(matrix1) == 38
        processService.getSumSideDiagonal(matrix2) == 42
    }

    def "add sum to file"() {
        given:
        File file = new File("1_test.txt");
        file.write("qwe");
        processService.processFile(file);

        expect:
        file.getText().contains("sum =") == true
    }

    def "add file to count"() {
        given:
        File file = new File("a_test.txt");
        file.write("qaqaqaqa");
        processService.processFile(file);

        expect:
        file.getText().contains("count = 4") == true
    }
}
